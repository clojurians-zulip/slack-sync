# slack sync

Unidirectional syncing from Clojurians Slack to [Clojurians Zulip's #slack-archive](https://clojurians.zulipchat.com/#narrow/stream/180378-slack-archive).

## usage

## setup

```bash
$ cp config.edn{.sample,}
# check settings
$ $EDITOR config.edn
```

### running

```bash
$ ./slack_zulip_sync.clj --env prod --config config.edn
```

### tests

``` bash
# 'run once'
$ ./scripts/ohmyclj test slack_zulip_sync.clj
# or
./scripts/ohmyclj repl slack_zulip_sync.clj
user=> (do (reload) (run-tests)) ;; change code, rince repeat
```

## ohmyclj/inclined

This comes with a vendored-(pre-release) of [ohmyclj](https://gitlab.com/eval/ohmyclj/tree/master#ohmyclj). This version of ohmyclj detects [inclined](https://gitlab.com/eval/inclined/tree/master#inclined) and calls `user/-main` accordingly.
