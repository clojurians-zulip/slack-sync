#!/usr/bin/env scripts/ohmyclj
"DEPS='https://gitlab.com/eval/inclined.git=18202f0c4aa69ca352ec5aaf5847047bc5004c8d;aleph/aleph=0.4.6;cheshire/cheshire=5.10.0;org.clojure/core.async=1.3.610'"

(ns user
  (:require [aleph.http :as http]
            [cheshire.core :as json]
            [clojure.core.async :as async :refer [go-loop <!]]
            [clojure.edn :as edn]
            [clojure.string :as string]
            [manifold.stream :as s]))

;; util

(defn log [& args]
  (apply println (str (java.time.LocalDateTime/now)) args))

(defn pp-id [id]
  (format "%.06f" id))

(def sent-by-ts (atom {}))

(defn store-sent [{ts :ts :as _slack-event} zulip-resp]
  (when-let [zulip-id (-> zulip-resp :body :id)]
    (let [slack-id (read-string ts)]
      (log :info :store-sent (pr-str [slack-id zulip-id]))
      (swap! sent-by-ts update slack-id (fnil conj []) zulip-id))))


;; SOURCE https://gist.github.com/danielpcox/c70a8aa2c36766200a95#gistcomment-2711849
(defn deep-merge [& maps]
  (apply merge-with (fn [& args]
                      (if (every? map? args)
                        (apply deep-merge args)
                        (last args)))
         maps))


(defn fetch-username [token uid]
  (-> @(http/get "https://slack.com/api/users.info" {:as :json :query-params {:token token
                                                                              :user uid}})
      :body
      (get-in [:user :name])))

(def mem-fetch-username (memoize fetch-username))


(defn fetch-channelname [token cid]
  (-> @(http/get "https://slack.com/api/conversations.info" {:as :json :query-params {:token token
                                                                                      :channel cid}})
      :body
      (get-in [:channel :name])))

(def mem-fetch-channelname (memoize fetch-channelname))


(defn fetch-url [token]
  (-> @(http/post "https://slack.com/api/rtm.connect" {:as :json :form-params {:token token}})
      :body
      :url))

(defn connect [url]
  @(http/websocket-client url))


;; SOURCE https://github.com/clojureverse/clojurians-log-app/blob/master/src/clj/clojurians_log/message_parser.clj
(defn- replace-html-entities
  "Replace `&amp;`, `&lt;`, and `&gt;` with \"&\", \"<\", and \">\".
  This seems equired as per https://api.slack.com/docs/message-formatting"
  [message]
  (-> message
      (string/replace #"&amp;" "&")
      (string/replace #"&lt;" "<")
      (string/replace #"&gt;" ">")))


(defn replace-emoji [text]
  (let [emoji-re #"(?<!\w):(\w*?)(::skin-tone[^:]+)?:"
        mapping  {"thumbsup"              "thumbs_up"
                  "thumbsup_all"          "thumbs_up"
                  "slightly_smiling_face" "slight_smile"
                  "parrot"                "party-parrot"
                  "ok_hand"               "ok"
                  "white_check_mark"      "check"
                  "heavy_check_mark"      "check_mark"
                  "ballot_box_with_check" "checkbox"
                  "robot_face"            "robot"
                  "metal"                 "rock_on"
                  "clj"                   "clojure"
                  "cljs"                  "clojurescript"
                  "thinking_face"         "thinking"}] ;; slack->zulip
    (reduce (fn [acc [match emoji _skin]]
              (let [new-emoji (str ":" (get mapping emoji emoji) ":")]
                (string/replace acc match new-emoji)))
            text
            (re-seq emoji-re text))))


;; re's via clojurians-log-app
(defn- fix-styles
  "Slack uses different bold, italic and strike-through formatting"
  [text]
  (let [bold-re      #"(?<![a-zA-Z0-9`])\*(.*?)\*(?![a-zA-Z0-9`])"
        italic-re    #"\b_(.*?)_"
        strike-re    #"~(.*?)~"
        url-re       #"<((?:http|https):[^|]*?)(?:\|(.*?))?>"
        fmt-url      (fn [[_matched url title]]
                       (if title
                         (str "[" title "](" url ")")
                         url))]
    (-> text
        (string/replace bold-re "**$1**")
        (string/replace italic-re "*$1*")
        (string/replace strike-re "~~$1~~")
        (string/replace url-re fmt-url))))


(defn resolve-user-refs [text user-resolver]
  (let [user-ref-re #"<@(U[A-Z0-9]{7,})>"]
    (reduce (fn [acc [match uid]]
              (let [user-ref (str "*@" (user-resolver uid) "*")]
                (string/replace acc match user-ref)))
            text
            (re-seq user-ref-re text))))


(defn resolve-channel-refs [text]
  (let [channel-ref-re #"<(#C[A-Z0-9]{7,})(?:\|(.*?))?>"]
    (reduce (fn [acc [match _chid chname]]
              (string/replace acc match (str "*#" chname "*")))
            text
            (re-seq channel-ref-re text))))


(defn format-text [text {user-resolver :user-resolver}]
  (log :info :format-text (pr-str text))
  (-> text
      (replace-html-entities)
      (fix-styles)
      (resolve-channel-refs)
      (resolve-user-refs user-resolver)
      (replace-emoji)
      ;; text will be prepended with sender's name
      ;; any codeblock in begin should therefor move to newline
      (string/replace #"^```" "\n```")
      ;; codeblock does not start with newline, eg "\n```my code\n"
      (string/replace #"\n```([^\n]+)\n?" "\n```\n$1\n")
      ;; if it looks like edn, or a repl -> clojure
      (string/replace #"\n```\n?( *\{| *\[| *\(|user=>|cljs.user=>)" "\n```clojure\n$1")
      ;; Zulip requires a newline between code and closing ```
      ;; or code block is not terminated
      (string/replace #"(.+)```\n?" "$1\n```\n")
      ;; 'clj' or a '$' => shell
      (string/replace #"\n```\n( *clj| *\$)" "\n```shell\n$1")))


(defn slack-event->zulip-message
  "(slack-event->zulip-message ..) => {:stream ... :topic ... :content ...}"
  [{slack-cfg :slack zulip-cfg :zulip :as _config} {:keys [user text channel] :as event}]
  (log :info :sending-event (pr-str event))
  (let [user-resolver  (partial mem-fetch-username (:token slack-cfg))
        formatted-text (format-text text {:user-resolver user-resolver})
        topic          (mem-fetch-channelname (:token slack-cfg) channel)
        username       (mem-fetch-username (:token slack-cfg) user)
        content        (str "**" (or username user) ":** " formatted-text)]
    {:stream (:main-stream zulip-cfg) :topic topic :content content}))


(defn announcify
  "Yields zulip-msg suitable for announcement"
  [{{:keys [announce-stream announce-topic]} :zulip} zulip-msg]
  (merge zulip-msg {:stream announce-stream :topic announce-topic}))


(defn post-zulip-message [{auth :auth :as _cfg} {:keys [stream topic content]}]
  (log :info :post-zulip-message :stream stream :topic topic :content content)
  (let [params {:type    "stream"
                :to      stream
                :subject topic
                :content content}]
    (http/post "https://clojurians.zulipchat.com/api/v1/messages"
               {:as          :json
                :basic-auth  auth
                :form-params params})))


(defn delete-zulip-message [{auth :auth :as _cfg} id]
  (log :info :delete-zulip-message :id id)
  (http/delete (str "https://clojurians.zulipchat.com/api/v1/messages/" id)
               {:basic-auth auth}))

(defn change-zulip-message [{auth :auth :as _cfg} {:keys [id content]}]
  (log :info :change-zulip-message :id id :content content)
  (let [params {:content content}]
    (http/patch (str "https://clojurians.zulipchat.com/api/v1/messages/" id)
                {:as          :json
                 :basic-auth  auth
                 :form-params params})))

(defn generate-event-handler [& handlers]
  (let [parse-event #(-> % (json/parse-string true))]
    (fn [event]
      (let [parsed (parse-event event)]
        (log :info :received-event (pr-str parsed))
        (doall (map #(% parsed) handlers))))))


(defn init-send-loop! [ch {zulip-cfg :zulip :as config}]
  (go-loop []
    (log :info :send-loop! :waiting-for-event)
    (when-let [event (<! ch)]
      (log :info :send-loop! :event-received)
      (let [zulip-msg (slack-event->zulip-message config event)]
        (try
          (some->> zulip-msg
                   (post-zulip-message zulip-cfg)
                   (deref)
                   (store-sent event))
          (catch Exception e
            (log :error :sending-failed :exception e))))
      (log :info :send-loop! :event-done)
      (recur))))


(defn init-copy-loop! [ch {zulip-cfg :zulip :as config}]
  (go-loop []
    (log :info :copy-loop! :waiting-for-event)
    (when-let [event (<! ch)]
      (log :info :copy-loop! :event-received)
      (let [zulip-msg   (slack-event->zulip-message config event)
            copy-stream (get (:channel-copies zulip-cfg) (:channel event))
            zulip-msg   (assoc zulip-msg :stream copy-stream :topic "Slack archive")]
        (try
          (some->> zulip-msg
                   (post-zulip-message zulip-cfg)
                   (deref)
                   (store-sent event))
          (catch Exception e
            (log :error :sending-failed :exception e))))
      (log :info :copy-loop! :event-done)
      (recur))))


(defn init-announce-loop! [ch {zulip-cfg :zulip :as config}]
  (go-loop []
    (log :info :announce-loop! :waiting-for-event)
    (when-let [event (<! ch)]
      (let [zulip-msg (announcify config (slack-event->zulip-message config event))]
        (try
          (some->> zulip-msg
                   (post-zulip-message zulip-cfg)
                   (deref)
                   (store-sent event))
          (catch Exception e
            (log :error :announce-failed :exception e)))
        (log :info :announce-loop! :event-done))
      (recur))))


(defn init-delete-loop! [ch {zulip-cfg :zulip :as _config}]
  (go-loop []
    (log :info :delete-loop! :waiting-for-event)
    (when-let [event (<! ch)]
      (let [slack-id (-> event :deleted_ts read-string)]
        (log :info :delete-loop! :searching-messages :slack-id (pp-id slack-id))
        (when-let [zulip-ids (-> sent-by-ts deref (get slack-id))]
          (log :info :delete-loop! :zulip-ids (pr-str zulip-ids))
          ;; when not dereffing the http-delete, we don't need a try/catch
          (doall (map (partial delete-zulip-message zulip-cfg) zulip-ids))))
      (recur))))

(defn- successful-zulip-response [zulip-response]
  (if (= (-> zulip-response :body :result) "success")
    zulip-response
    (throw
     (ex-info "Zulip API call failed" (-> zulip-response :body)))))

(defn init-change-loop! [ch {zulip-cfg :zulip :as config}]
  (go-loop []
    (log :info :change-loop! :waiting-for-event)
    (when-let [event (<! ch)]
      (let [slack-id (-> event :message :ts read-string)]
        (log :info :change-loop! :searching-messages :slack-id (pp-id slack-id))
        (when-let [zulip-ids (-> sent-by-ts deref (get slack-id))]
          (log :info :change-loop! :zulip-ids (pr-str zulip-ids))
          (doseq [id zulip-ids]
            (let [zulip-msg (slack-event->zulip-message config (:message event))]
              (try
                (some-> zulip-msg
                        (select-keys [:content])
                        (assoc :id id)
                        (->> (change-zulip-message zulip-cfg))
                        (deref)
                        (successful-zulip-response))
                (catch Exception e
                  (log :error :changing-failed :exception e)))))))
      (recur))))

(defn load-config [path {env :env}]
  (let [config     (-> path slurp edn/read-string)
        env-config (get config env)]
    (deep-merge config env-config)))


(defn event-xf [{{:keys [ignore-channels]} :slack} & xfs]
  (let [non-ignored #(->> % :channel ignore-channels not)
        default-xf  (filter non-ignored)]
    (apply comp default-xf xfs)))


(def ^:private async-mult (memoize async/mult))


(defn- tap-ch [from-ch new-ch]
  (async/tap (async-mult from-ch) new-ch))


(defn consume [{slack-cfg               :slack
                {:keys [channel-copies]} :zulip :as config}]
  (let [without-subtype    #(not (contains? % :subtype))
        thread-broadcasts  #(-> % :subtype (= "thread_broadcast"))
        message?           (every-pred #(-> % :type (= "message"))
                                       (some-fn without-subtype
                                                thread-broadcasts))
        needs-copying?     (every-pred message? #(some->> % :channel
                                                          (get channel-copies)))
        announcement?      (every-pred message?
                                       #(-> % :channel (= "C06MAR553"))
                                       #(-> % :thread_ts nil?))
        delete-msg-event?  (every-pred #(-> % :deleted_ts some?)
                                       #(-> % :subtype (= "message_deleted")))
        change-msg-event?  (every-pred #(-> % :message some?)
                                       #(-> % :subtype (= "message_changed")))
        incoming-events-ch (async/chan)
        to-send-ch         (tap-ch incoming-events-ch
                                   (async/chan (async/sliding-buffer 4)
                                               (event-xf config (filter message?))))
        to-copy-ch         (tap-ch incoming-events-ch
                                   (async/chan (async/sliding-buffer 4)
                                               (event-xf config (filter needs-copying?))))
        to-announce-ch     (tap-ch incoming-events-ch
                                   (async/chan (async/sliding-buffer 4)
                                               (event-xf config (filter announcement?))))
        to-delete-ch       (tap-ch incoming-events-ch
                                   (async/chan (async/sliding-buffer 4)
                                               (event-xf config (filter delete-msg-event?))))
        to-change-ch       (tap-ch incoming-events-ch
                                   (async/chan (async/sliding-buffer 4)
                                               (event-xf config (filter change-msg-event?))))]
    (log :info :main)
    (init-send-loop! to-send-ch config)
    (init-copy-loop! to-copy-ch config)
    (init-announce-loop! to-announce-ch config)
    (init-delete-loop! to-delete-ch config)
    (init-change-loop! to-change-ch config)

    (s/consume (generate-event-handler (partial async/put! incoming-events-ch))
               (connect (fetch-url (:token slack-cfg))))))


(defn ^#:inclined{:option.env    {:desc    "environment that will load parts from config.edn"
                                  :default "dev"}
                  :option.config {:desc      "path to config.edn"
                                  :required? true}}
  -main
  "Capture events from slack and send them to Zulip"
  [{config-file :config env :env}]
  {:pre [(#{"dev" "prod"} env)]}
  (let [config (load-config config-file {:env (keyword env)})]
    (log :info :main)

    (loop []
      (try
        @(consume config)
        (catch Exception e
          (log :error :consume :exception e)))
      (log :error :connection-lost :reconnecting)
      (Thread/sleep 2000)
      (recur))))

(ns test
  (:require [clojure.test :refer [deftest testing are]]
            [user]))

(deftest format-text-test
  (let [fut #(user/format-text %1 {:user-resolver identity})]
    (testing "code and triple-ticks should always have a newline between them"
      (are [input expected] (= expected (fut input))
        "```(def a 1)```"
        "
```clojure
(def a 1)
```
"

        "```(def a 1)```\nNot code"
        "
```clojure
(def a 1)
```
Not code"

        "```Foobar```"
        "
```
Foobar
```
"
        ))))
